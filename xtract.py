import os
import datetime as dt
import re
import getpass

##CHANGE THIS:
name_to_find = "marek.pleskac"

##DON'T CHANGE THIS:
now=dt.datetime.now()
email_to_find = name_to_find + "@oracle.com"
logs_path = os.path.join(os.getenv('APPDATA'),".Purple\\logs\\jabber\\", email_to_find)
sme_chat = "od_sme@conference.oracle.com.chat"
tech_chat = "od_techassist@conference.oracle.com.chat"

##REGEX DEF
re_chat_response = "^(?!.*Conversation).*?(" + name_to_find + ").*?$"
re_rfc = ".*?(([0-9])-([0-9]|[a-z]|[A-Z]){7}).*?"
re_sr = ".*?(3-([0-9]){11,13}).*?"



class chat_xtract(object):
	def __init__(self):
		pass

	def find_files(self, no_days, chat):
		file_list = []
		ago=now-dt.timedelta(days=no_days)
		chat_path = logs_path + "\\" + chat
		for root, dirs, files  in os.walk(chat_path):
			for file_name in files:
				full_file_path = os.path.join(chat_path,file_name)
				file_stat = os.stat(full_file_path)
				ctime = dt.datetime.fromtimestamp(file_stat.st_ctime)
				if ctime > ago:
					file_list.append(full_file_path)
					self.check_file_rfc(full_file_path, ctime)
					#print('%s modified %s'%(full_file_path,ctime))
		return file_list

	def check_file_rfc(self, file_path, ctime):
		regex = re.compile(re_chat_response)
		rfc = re.compile(re_rfc)
		try:
			f = open(file_path)
			found = False
			for line in f:
				#print("MATCHING: " + line)
				#m = re.search('^(?!.*Conversation).*?(marek.pleskac).*?$', line)
				#m = re.search('^.*?(Conversation).*?(marek.pleskac).*?$', line)
				#m = re.search('^.*?(marek.pleskac).*?$', line)
				#print(m)
				m = regex.search(line)
				if m:
					found = True
					file_line = rfc.search(line)
					if file_line:
						rfc_no = file_line.group(1)
						#print("RFC NO: " + str(rfc_no))
						self.build_csv_line(self.make_date(ctime),rfc_no)
					else:
						pass
						#print("NON RFC RESPONSE:")
					#print('Found in ' + str(ctime))
					#print(line)
		except IOError:
			print("File doesn't exists")

#Accepts timestamp (2016-08-02 10:38:23.495254) and returns list ([02.08.2016,10])
#Start time not yet being used
	def make_date(self,timestamp):
		time_out = []
		date_time = str(timestamp).split()
		date_tokens = date_time[0].split("-")
		time_tokens = date_time[1].split(":")
		final_date = date_tokens[2] + "." + date_tokens[1] + "." + date_tokens[0]
		time_out.append(final_date)
		time_out.append(time_tokens[0])
		return time_out

##,3-5XDBP6N,N/A,Incident Management,90,27.07.2016,
	def build_csv_line(self,rfc_date,rfc_number):
		#print(rfc_date[0])
		#print(rfc_number)
		csv_line = ("," + rfc_number + ",N/A,Incident Management,<DURATION>," + rfc_date[0] + ",")
		print(csv_line)


xtract = chat_xtract()
xtract.find_files(7, sme_chat)
